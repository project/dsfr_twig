<?php

/**
 * @file
 * Contains \Drupal\dsfr_twig\Twig\RenderBlock.
 */

namespace Drupal\dsfr_twig\Twig;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Symfony\Component\Mime\MimeTypes;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Adds extension to render a block.
 */
class DsfrTwig extends AbstractExtension
{

  private $utf8 = 'UTF-8';

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'dsfr_twig';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions()
  {

    return [

      new TwigFunction(
        'dsfr_download',
        [$this, 'dsfr_download'],
        ['is_safe' => ['html']]
      ),
      new TwigFunction(
        'get_media_info',
        [$this, 'get_media_info'],
        ['is_safe' => ['html']]
      ),
      new TwigFunction(
        'limit_string',
        [$this, 'limit_string'],
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * DSFR download file (link) 
   */
  public function dsfr_download( $mid, $wrapper = 0, $args = [] ) {

    if ( is_numeric( $mid ) ) {

      $file = $this->get_media_info( $mid );
  
      // Card
      if( $wrapper == 1 ) {
  
        return '      
        <div class="fr-card fr-enlarge-link fr-card--download">
          <div class="fr-card__body">
            <div class="fr-card__content">
              <h3 class="fr-card__title">
                <a download href="'. $file['url'] .'">
                '. t('Download the document') .'  '. $file['name'] .'
                </a>
              </h3>
              <p class="fr-card__desc"></p>
              <div class="fr-card__end">
                <p class="fr-card__detail">'. $file['ext'] .' – '. $file['size'] .'</p>
              </div>
            </div>
          </div>
        </div>';
  
      // Tile (with picto)
      } elseif ( $wrapper == 2 ) {

        $lib = base_path() . 'themes/contrib/dsfr/dist/dsfr/';
        // to do, add: '. t('Detail (optional)') .'
  
        return '
        <div class="fr-tile fr-tile--download fr-enlarge-link" id="tile-'.time().'">
          <div class="fr-tile__body">
            <div class="fr-tile__content">
              <h3 class="fr-tile__title">
                <a href="'. $file['url'] .'" download>'. t('Download the document') .' '. $file['name'] .'</a>
              </h3>
              <p class="fr-tile__detail"></p>
            </div>
          </div>
          <div class="fr-tile__header">
            <div class="fr-tile__pictogram">
              <svg aria-hidden="true" class="fr-artwork" viewbox="0 0 80 80" width="80px" height="80px">
                <use class="fr-artwork-decorative" href="'. $lib .'artwork/pictograms/buildings/city-hall.svg#artwork-decorative"></use>
                <use class="fr-artwork-minor" href="'. $lib .'artwork/pictograms/buildings/city-hall.svg#artwork-minor"></use>
                <use class="fr-artwork-major" href="'. $lib .'artwork/pictograms/buildings/city-hall.svg#artwork-major"></use>
              </svg>
            </div>
          </div>
        </div>';
  
      // Link
      } else {
  
        return '    
        <a class="fr-link fr-link--download" download href="'. $file['url']  .'">
          '. $file['name'] .'
          <span class="fr-link__detail">'. $file['ext'] .' – '. $file['size'].'</span>
        </a>
        ';
      }
    
    } else {

      return t('The MID should be an number');
    }
  }



  /**
   * Limiting a number of characters
   */
  public function limit_string($str = '', $limit = 60)
  {

    return (strlen($str) > $limit) ? mb_substr($str, 0, $limit, $this->utf8) . '[...]' : $str;
  }

  /**
   * Convert a file size to make it more readable
   */
  public function convert_bytes( $octet )
  {

    $unite = array('octet', 'ko', 'mo', 'go');

    if ($octet < 1000) // octet
    {
      return $octet . $unite[0];
    } else {
      if ($octet < 1000000) // ko
      {
        $ko = round($octet / 1024, 2);
        return $ko . $unite[1];
      } else // Mo ou Go 
      {
        if ($octet < 1000000000) // Mo 
        {
          $mo = round($octet / (1024 * 1024), 2);
          return $mo . $unite[2];
        } else // Go 
        {
          $go = round($octet / (1024 * 1024 * 1024), 2);
          return $go . $unite[3];
        }
      }
    }
  }

  /**
   * Get file information from a media ID
   */
  public function get_media_info( $mid )
  {

    $media = Media::load($mid);
    $fid = $media->getSource()->getSourceFieldValue($media);
    $file = File::load($fid);
    $mime = $file->getMimeType();

    $type = New MimeTypes();
    $ext = $type->getExtensions( $mime );

    return [
      'name' => $media->getName(),
      'url' => $file->createFileUrl(),
      'size' => $this->convert_bytes( $file->getSize() ),
      'mime' => $mime,
      'ext' => strtoupper( $ext[0] ),
    ];
  }
}